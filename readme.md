```
git clone https://gitlab.com/common.helm/kube.git
cd kube
helm create basic-nginx
cd basic-nginx
rm -rf templates/*
vi templates/basic-nginx.yaml
vi Chart.yaml
rm values.yaml
cd ..
helm package basic-nginx/
helm repo index .
git add .
git commit -m 'helm repo inited'
git push origin master
helm repo add myrepo  https://gitlab.com/common.helm/kube/raw/master/
helm search nginx
helm repo list
helm repo update
helm install myrepo/basic-nginx --name basic-nginx
```
